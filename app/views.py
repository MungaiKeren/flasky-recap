from flask import render_template
from app import app
from .request import get_movies

# our first views
@app.route('/')
def index():
    '''
    view root function that will render information to the index page
    '''
    title = 'Welcome to the best movie review website online'


    # getting popular, upcoming and now playing movies
    popular_movies = get_movies('popular')
    upcoming_movie = get_movies('upcoming')
    now_showing_movie = get_movies('now_playing')
    return render_template('index.html', title=title, popular= popular_movies, 
        upcoming_movie=upcoming_movie, now_showing_movie=now_showing_movie)


@app.route('/movie/<int:movie_id>')
def movie(movie_id):
    '''
    view movie function that will return a movie page and its contents
    '''
    return render_template('movie.html', id=movie_id)